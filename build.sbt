name := "simple-singup-service"

val globalSettings = Seq[SettingsDefinition](
  version := "0.1",
  scalaVersion := "2.12.6"
)

val persistence = project.in(file("persistence-module"))
  .settings(globalSettings: _*)
  .settings(
    libraryDependencies ++= Seq(
      "org.postgresql" % "postgresql" % "42.1.4",
      "com.typesafe.slick" %% "slick" % "3.2.1",
      "org.slf4j" % "slf4j-nop" % "1.6.4",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.2.1"
    ),
    assemblyOutputPath in assembly := file(Option("docker/persistence/").getOrElse("") + "persistence-module-" + version.value + ".jar")
  )

val signup = project.in(file("singup-module"))
  .settings(globalSettings: _*)
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % "10.1.1",
      "com.typesafe.akka" %% "akka-stream" % "2.5.11"
    ),
    assemblyOutputPath in assembly := file(Option("docker/signup/").getOrElse("") + "singup-module-" + version.value + ".jar")
  )